<?php

$wgEnableUploads  = true;

#############
# WIkiEditor

wfLoadExtension( 'WikiEditor' );

# Enables use of WikiEditor by default but still allows users to disable it in preferences
$wgDefaultUserOptions['usebetatoolbar'] = 1;

# Enables link and table wizards by default but still allows users to disable them in preferences
$wgDefaultUserOptions['usebetatoolbar-cgd'] = 1;

# Displays the Preview and Changes tabs
$wgDefaultUserOptions['wikieditor-preview'] = 1;

# Displays the Publish and Cancel buttons on the top right side
#$wgDefaultUserOptions['wikieditor-publish'] = 1;

###########
# TOC

$wgMaxTocLevel = 3;
#$wgDefaultUserOptions['numberheadings'] = 1;



#$wgAllowUserCss = true;

#require_once( "$IP/skins/vectorfablabo/vectorfablabo.php" );

#require_once( "$IP/skins/LoTest/LoTest.php" );


## Default skin: you can change the default skin. Use the internal symbolic
## names, ie 'vector', 'monobook':
#$wgDefaultSkin = "lotest";
$wgDefaultSkin = "vectorfablabo";

# Enabled skins.
# The following skins were automatically enabled:
wfLoadSkin( 'CologneBlue' );
wfLoadSkin( 'Modern' );
wfLoadSkin( 'MonoBook' );
wfLoadSkin( 'Vector' );
wfLoadSkin( 'LoTest' );
#wfLoadSkin( 'Fablabo' );
wfLoadSkin( 'VectorFablabo' );


# End of automatically generated settings.
# Add more configuration options below.

# Confirm Account
require_once "$IP/extensions/ConfirmAccount/ConfirmAccount.php";
$wgMakeUserPageFromBio = true;
$wgAutoWelcomeNewUsers = true;
$wgConfirmAccountRequestFormItems = array(
        'UserName'        => array( 'enabled' => true ),
        'RealName'        => array( 'enabled' => false ),
        'Biography'       => array( 'enabled' => true, 'minWords' => 20 ),
        'AreasOfInterest' => array( 'enabled' => false ),
        'CV'              => array( 'enabled' => false ),
        'Notes'           => array( 'enabled' => false ),
        'Links'           => array( 'enabled' => true ),
        'TermsOfService'  => array( 'enabled' => false ),
 );

# ParserFuntions : nothing to install
wfLoadExtension( 'ParserFunctions' );
$wgPFEnableStringFunctions = true;

# Mobile Frontend
wfLoadExtension( 'MobileFrontend' );
$wgMFAutodetectMobileView = true;

# Semantic Mediawiki
#include_once("$IP/extensions/SemanticMediaWiki/SemanticMediaWiki.php");
enableSemantics( 'localhost' );

# Semantic Forms
include_once("$IP/extensions/SemanticForms/SemanticForms.php");

# Semantic Forms Input
require_once("$IP/extensions/SemanticFormsInputs/SemanticFormsInputs.php");

# Semantic Drilldown
include_once("$IP/extensions/SemanticDrilldown/SemanticDrilldown.php");

# Semantic Compound queries
require_once( "$IP/extensions/SemanticCompoundQueries/SemanticCompoundQueries.php" );

# Semantic Internal objetcts
include_once("$IP/extensions/SemanticInternalObjects/SemanticInternalObjects.php");

require_once( "$IP/extensions/CategoryTree/CategoryTree.php" );

require_once "$IP/extensions/Widgets/Widgets.php";

# menu sidebar collapsible
wfLoadExtension( 'CollapsibleVector' );

# Spoilers
require_once "$IP/extensions/Spoilers/Spoilers.php";

#wfLoadExtension( 'MsUpload' );
require_once "$IP/extensions/MsUpload/MsUpload.php";
$wgMSU_useDragDrop = true; // Should the drag & drop area be shown? (Not set by default)
$wgMSU_imgParams = '700px'; // The default parameters for inserted images

# MATH
require_once "$IP/extensions/Math/Math.php";
#$wgMathUseLaTeXML = true;

// ensure 'mathml'; is added to the $wgMathValidModes array;
#$wgMathValidModes[] = 'mathml';
$wgTexvc = '/usr/bin/texvc';

// Set Mathoid as default rendering option;
#$wgDefaultUserOptions['math'] = 'mathml';

$wgMathValidModes = array(MW_MATH_MATHJAX); // Define MathJax as the only valid math rendering mode
$wgUseMathJax = true; // Enable MathJax
$wgDefaultUserOptions['math'] = MW_MATH_MATHJAX; // Set MathJax as the default rendering option
$wgDefaultUserOptions['mathJax'] = true; // Enable the MathJax checkbox option
$wgHiddenPrefs[] = 'math'; // Hide math preference
$wgHiddenPrefs[] = 'mathJax'; // Hide MathJax checkbox
$wgMathDisableTexFilter = true; // or compile "texvccheck"

// Set MathML as default rendering option
#$wgDefaultUserOptions['math'] = 'mathml';
#$wgMathFullRestbaseURL= 'https://api.formulasearchengine.com/';

// Meta OpenGraph
//require_once( "$IP/extensions/OpenGraphMeta/OpenGraphMeta.php" );
require_once "$IP/extensions/WikiSEO/WikiSEO.php";

// Modification du menu de gauche
// Licence WTFPL 2.0
$wgHooks['BaseTemplateToolbox'][] = 'modifyToolbox';

function modifyToolbox( BaseTemplate $baseTemplate, array &$toolbox ) {

	static $keywords = array( 'WHATLINKSHERE', 'RECENTCHANGESLINKED', 'FEEDS', 'CONTRIBUTIONS', 'LOG', 'BLOCKIP', 'EMAILUSER', 'USERRIGHTS', 'UPLOAD', 'SPECIALPAGES', 'PRINT', 'PERMALINK', 'INFO' );

	$modifiedToolbox = array();

	// Walk in the MediaWiki:Sidebar message, section toolbox
	foreach ( $baseTemplate->data['sidebar']['TOOLBOX'] as $value ) {
		$specialLink = false;
  
		// Search if the keyword exists
		foreach ( $keywords as $key ) {

			if ( $value['href'] == Title::newFromText($key)->fixSpecialName()->getLinkURL() ) {
				$specialLink = true;

				// This is a keyword, hence add this special link
				if ( array_key_exists( strtolower($key), $toolbox ) ) {
					$modifiedToolbox[strtolower($key)] = $toolbox[strtolower($key)];
					break;
				}
			}
		}

		// This is a normal link
		if ( !$specialLink ) {
			$modifiedToolbox[] = $value;
		}
	}

    $modifiedToolbox += $toolbox;
	$toolbox = $modifiedToolbox;

	return true;
}


// essai d'ajouter soi meme les balises meta open graph mais c'est chiany
// function onBeforePageDisplayMeta( OutputPage &$out, Skin &$skin ) {

//     if (in_array('Projets', $out->getCategories())){
//         $value = "";
//         $name='og:image';
//         $out->addMeta( $name, $value );
//     }
//     //echo('toto');
//     return true;
// }

// $wgHooks['BeforePageDisplay'][] = 'onBeforePageDisplayMeta';


// PHP Errors
error_reporting( -1 );
ini_set( 'display_errors', 1 );
$wgDebugToolbar = true;
 
// meta hook


