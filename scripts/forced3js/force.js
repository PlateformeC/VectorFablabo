var col = ["#ff7f0e", "#aec7e8", "#1f77b4" , "#2ca02c", "#ffbb78"];
var svg;
var tags = [];
var test_limite = 0;
var nn;

var width = jQuery(window).width();//1920;
var height = jQuery(window).height()-21;

function map_range(value, low1, high1, low2, high2) {
    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
}

function mapNodes(nodes) {
	var nodesMap;
	nodesMap = d3.map();
	nodes.forEach(function(n) {
	    return nodesMap.set(n.id, n);
	});
	return nodesMap;
    };

jQuery.noConflict();

jQuery(function($){
    var network = function(selection, data){
	
	var node, link;
	var curNodes, curLinks;
	var tooltip = Tooltip('vis-tooltip', 230);

	var hideDetails;

	var start_zoom = 0.4;
	var start_center_x = -(width/2)*(start_zoom-1);
	var start_center_y = -(height/2)*(start_zoom-1);

	var linkedByIndex = {};
	var nodesMap = mapNodes(data.nodes);

	data.links.forEach(function(l) {
	    //l.source = ;
	    //l.target = nodesMap.get(l.target);
	    return linkedByIndex["" + nodesMap.get(l.source).id + "," + nodesMap.get(l.target).id] = 1;
	});


	var neighboring = function(a, b) {
 	    return linkedByIndex[a.id + "," + b.id] || linkedByIndex[b.id + "," + a.id];
	};


	var strokeFor = function(d) {
	    return d3.rgb(col[d.group]).darker().toString();
	};

	
	data.nodes.filter(function (d){
	    return d.group == 1;
	}).forEach(function (d){
	    if (d.id > test_limite){
		tags.push(d.name);
	    }
	});


	var color = d3.scale.category20();
	
	var force = d3.layout.force()
	    .charge(-1000)
	    .gravity(0.15)
	    .size([width, height]);

	var zoom = function() {
	    svg.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
	};

	svg = d3.select(selection).append("svg")
	    .attr("width", width)
	    .attr("height", height)
	    .attr("transform", "translate("+start_center_x+"," + start_center_y +")scale(" + start_zoom  + ")")
	    .call(d3.behavior.zoom().scaleExtent([0.2, 4]).on("zoom", zoom))
	.append('g');


	var update = function(){
	    var lst_nodes = [];
	    
	    curNodes = data.nodes.filter(function (d){
		if (d.group == 0){
		    return 1;
		}else{
		    return tags.indexOf(d.name)>=0;
		};
	    });

	    curNodes.forEach(function(d){
		lst_nodes.push(d.id);
	    });
	    
	    curLinks = data.links.filter(function(l){
		return lst_nodes.indexOf(l.source)>0 && lst_nodes.indexOf(l.target)>0;
	    });

	    link = svg.selectAll(".link")
		.data(curLinks);

	    link.enter().append("line")
		.attr("class", "link")
		.style("stroke-width", 4)
		.style("stroke-opacity", 1)
		.style("stroke-color", "#888");

	    link.exit()
		.transition()
		.duration(500)
		.attr("stroke-opacity", function(d){return 0;})
		.remove();

	    node = svg.selectAll(".node")
		.data(curNodes);

	    node.enter().append("circle")
		//.append("g")
		.attr("class", "node")
		//.append("circle")
	    	.attr("r", function(d){
	    	    return map_range(d.size, 0, 10, 10, 25);
	    	})
	    	.attr("cx", 0)//function(d){return d.x})
	    	.attr("cy", 0)//function(d){return d.y})
	    	.style("fill", function(d){return col[d.group]})
	    	.style("stroke", "#000").call(force.drag);
 		//.append("svg:text").text(function(d) {
		//    return d['name'];
		//}).style("fill", "#555").style("font", "Arial").style("font-size", 12)
	    
	    //node.append("text").text(function(d) {return d.name;});

	    // node.enter().append("svg:g")
	    // 	.attr("class", "node")
	    // 	.append('svg:text')
	    // 	.text(function(d) {
	    // 	   return d['name'];})
	    //  	.attr("x", function(d){return d.x})
	    //  	.attr("y", function(d){return d.y})
	    // 	.style("fill", function(d){return col[d.group]})
	    // 	.style("stroke", "#000")
	    // 	.style("opacity", "1")
	    // 	.call(force.drag);
		
	//    node.append("text").text(function(d) {
	//	   return d['name'];
	//	}).style("fill", "#555").style("font-family", "Arial").style("font-size", 12);
	    
	    node.on("mouseover", showDetails).on("mouseout", hideDetails);

	    force.nodes(data.nodes)
		.links(curLinks)
		.linkDistance(30)
		.linkStrength(0.6)
		// .linkStrength(
		//     function(l){
		// 	var ww = 1;
		// 	if (l.source.group != 0){
		// 	    ww = l.source.weight;
		// 	}else if (l.target.group != 0){
		// 	    ww = l.target.weight;
		// 	}
		// 	if (ww == 0){ww = 1;}
		// 	return 2/(ww);
		//     })
		.start();
	    
	    // node.append("title")
	    // 	.text(function(d) { return d.name; });
	    
	    force.on("tick", function() {
		link.attr("x1", function(d) { return d.source.x; })
		    .attr("y1", function(d) { return d.source.y; })
		    .attr("x2", function(d) { return d.target.x; })
		    .attr("y2", function(d) { return d.target.y; });
		
		node.attr("cx", function(d) { return d.x; })
		    .attr("cy", function(d) { return d.y; });
	    });

	    nn = svg.selectAll("node");

	    return node.exit()
		.transition()
		.duration(500)
		.attr("r", 0)
		.remove();

	};
	var showDetails = function(d, i) {
	    //if (link) {
		link.attr("stroke", function(l) {
		    if (l.source === d || l.target === d) {
			return "#fff";
		    } else {
			return "#333";
		    }
		}).attr("stroke-opacity", function(l) {
		    if (l.source === d || l.target === d) {
			return 1.0;
		    } else {
			return 0.8;
		    }
		});
	    //}
	    node.style("stroke", function(n) {
		if (n.searched || neighboring(d, n)) {
		    return "#fff";
		} else {
		    return strokeFor(n);
		}})
		.style("stroke-width", function(n) {
		    if (n.searched) {
			return 10.0;
		    } else if (neighboring(d, n)){
			return 2.0;
		    } else {
			return 1.0;
		    }
		});

	    var content = '<p class="main">' + d.name + '</p>';
	    if (d.group == 0){
		content += '<hr class="tooltip-hr">';
		content += '<p class="main">' + d.auteurs[0] + '</p>';
	    }
	    tooltip.showTooltip(content, d3.event);

	    return d3.select(this).style("stroke", "white").style("stroke-width", 4.0);
	};
	
	hideDetails = function(d, i) {
	    tooltip.hideTooltip();
	    node.style("stroke", function(n) {
		if (!n.searched) {
		    return strokeFor(n);
		} else {
		    return "#000";
		}
	    }).style("stroke-width", function(n) {
		if (!n.searched) {
		    return 1.0;
		} else {
		    return 10.0;
		}
	    });
	    if (link) {
		return link.attr("stroke", "#555").attr("stroke-opacity", 0.8);
	    }
	    return 0;
	};

	return update();
    };
    
    $.getJSON("data.json", function(data){
	network("#vis", data);


	// var tt = $('#vis').append($("<div/>", {
	//     "id":"tag-container",
	//     "class":"tag_list"
	// }));
	
	// tags.sort().forEach(function(d){
	//     var dd = $("<div/>",{
	// 	"class":"tag"
	//     });
	//     dd.text(d);
	//     $("#tag-container").append(dd);
	// });	
    });
});
       
