<?php
/**
 * SkinTemplate class for the VectorFablabo skin
 *
 * @ingroup Skins
 */
class SkinVectorFablabo extends SkinVector {
	public $skinname = 'vectorfablabo', $stylename = 'VectorFablabo',
		$template = 'VectorFablaboTemplate', $useHeadElement = true;

	/**
	 * Add CSS via ResourceLoader
	 *
	 * @param $out OutputPage
	 */
	public function initPage( OutputPage $out ) {

		$out->addMeta( 'viewport', 'width=device-width, initial-scale=1.0' );

		$out->addModuleStyles( array(
			'mediawiki.skinning.interface',
			'mediawiki.skinning.content.externallinks',
			'skins.vectorfablabo'
		) );
		$out->addModules( array(
			'skins.vectorfablabo.js'
		) );
	}

	/**
	 * @param $out OutputPage
	 */
	function setupSkinUserCss( OutputPage $out ) {
		parent::setupSkinUserCss( $out );
	}
}
