<noinclude>
Ceci est le formulaire « Tutoriel ».
Pour créer une page avec ce formulaire, entrez le nom de la page ci-dessous ;
si une page avec ce nom existe déjà, vous serez dirigé vers un formulaire pour l’éditer.


{{#forminput:form=Tutoriel}}

==références==
cette page est copiée de 
http://discoursedb.org/wiki/Special:FormEdit/Item/semantic_it's_easy

https://www.mediawiki.org/wiki/Extension:Semantic_Forms/Defining_forms#Multiple-instance_templates
-----

on fait référence à la notion de subobjet  :
https://www.semantic-mediawiki.org/wiki/Help:Adding_subobjects


==ajouter des images jolies===

pour faire apparaitre les images dans le formulaire on peut utiliser

https://www.mediawiki.org/wiki/Extension:Semantic_Image_Input

J'ai testé ça ce matin : ça marche pas des masses


</noinclude>
{{{info|add title=Créer un tutoriel|edit title=Modifier un Tutoriel}}}
<div id="wikiPreview" style="display: none; padding-bottom: 25px; margin-bottom: 25px; border-bottom: 1px solid #AAAAAA;"></div>
{{{for template|Tutoriel}}}

<div class="vf-form-entry"><div class="vf-form-entry-title">
Statut de la publication
</div><div class="vf-form-entry-description">
(Un choix)
</div><div class="vf-form-entry-input">
{{{field|status_pub|input type=listbox|values=Brouillon,Publié,Finalisé}}}
</div></div>

<div class="vf-form-entry"><div class="vf-form-entry-title">
Image
</div><div class="vf-form-entry-input">
{{{field|image|input type=text with autocomplete|property=image|uploadable|values from namespace=File}}}
</div></div>

<div class="vf-form-entry"><div class="vf-form-entry-title">
Description
</div><div class="vf-form-entry-description">
(Petite decription du tutoriel, 80 caractères max)
</div><div class="vf-form-entry-input">
{{{field|description|maxlength=80}}}
</div></div>

<div class="vf-form-entry"><div class="vf-form-entry-title">
Compétences requises
</div><div class="vf-form-entry-input">
{{{field|compétences requises|input type=text|list}}}
</div></div>

<div class="vf-form-entry"><div class="vf-form-entry-title">
License
</div><div class="vf-form-entry-input">
{{{field|license|input type=menuselect|structure={{MediaWiki:Licenses}}|default=CC-by-sa-3.0|Creative Commons Attribution CC-by-sa-3.0 France}}}
</div></div>

<div class="vf-form-entry"><div class="vf-form-entry-title">
Contributeurs
</div><div class="vf-form-entry-description">
(personnes participants à la rédaction, séparés par des virgules)
</div><div class="vf-form-entry-input">
{{{field|contributeurs|list|values from namespace=User}}} 
</div></div>

<div class="vf-form-entry"><div class="vf-form-entry-title">
Présentation
</div><div class="vf-form-entry-input">
{{{standard input|free text|editor=wikieditor}}}
</div></div>
{{{end template}}}

{{{for template|Etape|label=Etapes de ce tutoriel|multiple}}}
'''Titre de l'étape''' {{{field|Titre|1}}}

'''Images''' {{{field|Image|input type=text with autocomplete|uploadable|values from namespace=File|list}}}

'''Texte''' {{{field|Contenu|input type=textarea|editor=wikieditor}}}
{{{end template}}}


{{{standard input|minor edit}}} {{{standard input|watch}}}

{{{standard input|save}}} {{{standard input|preview}}} {{{standard input|changes}}} {{{standard input|cancel}}}