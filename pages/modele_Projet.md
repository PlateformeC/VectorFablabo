
<includeonly>
{{#seo:
|og:site_name=fablabo.net
|titlemode=append
|keywords=these,are,your,keywords
|description={{{description}}}
|og:image={{filepath:{{{image}}}}}
|og:type=article
}}

<div class="vf-cartouche-description">
[[description::{{{description|...}}}]]
</div>

[[category:Projets]]
<div id="vf-cartouche">
<div id="infos">
<div id="vf-cartouche-vignette">
{{#ifexist: Image:{{{image}}} |[[Image::Fichier:{{{image}}}| ]][[Image:{{{image}}}|340px]]|[[Image::Fichier:JoliAfficheur.jpg| ]][[Image:JoliAfficheur.jpg |340px]]}}
</div>
<div id="vf-cartouche-table">

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
Contributeur(s)
</div>  
<div class="vf-cartouche-infos-donnee">
{{#arraymap:{{{contributeurs|}}}|,|x|[[contributeur::User:x|x]]|\n}}
</div>
</div>

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
Status du projet
</div>  
<div class="vf-cartouche-infos-donnee">
{{{status}}}
</div>
</div>

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
Status de la publication
</div>  
<div class="vf-cartouche-infos-donnee">
{{{status_pub}}}
</div>
</div>

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
License
</div>  
<div class="vf-cartouche-infos-donnee">
{{{license}}}
</div>
</div>

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
Inspiration
</div>  
<div class="vf-cartouche-infos-donnee">
{{{inspiration|}}}
</div>
</div>

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
Fichiers source
</div>  
<div class="vf-cartouche-infos-donnee">
{{{source|}}}
</div>
</div>

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
Outils
</div>  
<div class="vf-cartouche-infos-donnee">
{{#arraymap:{{{machines|}}}|,|x|[[machine::x|x]]|\n}}
</div>
</div>

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
Ingrédients
</div>  
<div class="vf-cartouche-infos-donnee">
{{#arraymap:{{{ingrédients|}}}|,|x|[[ingrédient::x|x]]|\n}}
</div>
</div>

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
Lien
</div>  
<div class="vf-cartouche-infos-donnee">
{{{url| }}}
</div>
</div>


</div>
</div>
<div id="sommaire">
__TOC__
</div>
</div>
<br style="clear:both" />
</includeonly>
<noinclude>
<div class="MachineBox"   style="width:70%;float: left;">
==Taches effectuées dans le cadre de ce projet : ==

{{#ask:[[Category:Travaux]][[Projet::~{{PAGENAME}}]]
|?Description
|?usager
|?ingredient
|format=broadtable
|limit=10
|headers=show
|link=all
|class=sortable wikitable smwtable
|offset=
}}
</div>

=='''Documentation du modèle'''==
Un Boite d'infos pour résumer un projet
==Paramètres==
===en-tête===
*'''titre''' (requis)
*'''status''' (requis)
:{| class="wikitable"
!Status Type!!Theme Color
|-
|inconnu||style="background-color:#CCCCCC"|#CCCCCC
|-
|concept||style="background-color:#F3C741"|#F3C741
|-
|experimental||style="background-color:#FF3D09"|<span style="color:white"> #FF3D09</span>
|-
|fonctionne||style="background-color:#5F6E3C"|#5F6E3C
|-
|obsolete||style="background-color:#d3353c"|#d3353c
|-
|prototype||style="background-color:#284481"|#284481
|-
|abandoné||style="background-color:#463e34"|<span style="color:white"> #463e34</span>
|}
===Image===
*'''image''' (requis)
===General===
*'''description''' (requis)
*'''license''' (requis)
*'''contributeur(s)''' (requis)
*'''reprap''' (requirs)
*'''categories''' (requis)
*'''cadModel''' (required)
*'''url''' (required)

==Usage==
{| style="float: right"
| valign="top" | <pre>{{Development
<!--Header-->
|status = abandoned
<!--Image-->
|image = rustygears.jpeg
<!--General-->
|description = This is an example of the Development infobox template.
|license = GPL
|author = Example_User
|reprap = 
|categories = 
|cadModel = 
|url = 
}}
</pre>
|{{Development
<!--Header-->
|status = abandoned
<!--Image-->
|image = rustygears.jpeg
<!--General-->
|description = This is an example of the Development infobox template.
|license = GPL
|author = Example_User
|reprap = 
|categories = 
|cadModel = 
|url = 
}}
|}

=Todo=
* Change "Author" to "Contributors"
* Change "Based On" to "Related", otherwise sui-generis stuff looks funny
* Add "required" or "sub assemblies"? (need to think about this)

==Influences/Design==
The Goal of this page is to provide a template to categorize and organize RepRap development in a similar way that MediaWiki categorizes it's extensions (see the extension template in use [http://www.mediawiki.org/wiki/Extension:Add_Metas here]). Unfortunately for the time being we do not have a recent enough version of MediaWiki to do some of the cooler auto-categorization things that template does. This is what the extension template looks like:

<pre>{{Extension|templatemode =
|name        = Add Metas
|status      = experimental
|type1       = example
|type2       =
|hook1       =
|hook2       =
|username    =
|author      = Luis Diaz
|description =This extension enables you to add meta tags on all the pages like meta keywords.
|image       =
|imagesize   =
|version     =0.1
|update      =
|mediawiki   =Tested on 1.10.0
|license     =
|download    =[http://www.buntulug.com.ar/wiki/Add_Metas buntulug.com.ar]
|readme      =
|changelog   =
|parameters  =
|rights      =
|example     =
}}</pre>

==See Also==
* [[Development]]
* [[:Category: Development]]
[[Category:Template]]
</noinclude>
<noinclude>[[category:datafab]]</noinclude>
