__NOTOC__
<div class="vf-liens-accueil">
<div class="vf-lien-accueil">[[Formulaire:Projet|Créer un projet]]</div>
<div class="vf-lien-accueil">[[Formulaire:Tutoriel|Créer un tutoriel]]</div>
<div class="vf-lien-accueil">[[Aide:Accueil|Guide d'utilisation du wiki]]</div>
<div class="vf-lien-accueil">[[Aide:Documenter|Pourquoi documenter ?]]</div>
</div>

<br style="clear:both" />


'''Bienvenue sur le wiki Fablabo, animé par l'[http://www.pingbase.net association PiNG] !

Vous trouverez ici les projets partagés par les usagers de Plateforme C et de l'Atelier Partagé du Breil, ainsi que des ressources techniques et documentaires autour des fablabs.'''

==Projets à la une==

<div class="vf-liste-vignettes vf-liste-vignettes-3">
{{#ask:[[Category:EnAvant]]
 | limit=3
 | sort=Date de modification
 | order=descending
 | ?
 | ?Description
 | ?Image
 |format=template |template=ListeVignettes |headers=hide |link=none  |offset= }}
</div>

==Derniers projets modifiés==

<div class="vf-liste-vignettes vf-liste-vignettes-3">
{{#ask:[[Category:Projets]]
 | limit=3
 | sort=Date de modification
 | order=descending
 | ?
 | ?Description
 | ?Image
 |format=template |template=ListeVignettes |headers=hide |link=none  |offset= |searchlabel=|outro= voir tous les [[Projets]] }}
</div>

==Ressources==

{|width="700px" class="liensAccueil"
|class="enteteLiens"| <div>Documentation technique</div> ||class="enteteLiens"| <div>À propos des fablabs</div>
|-
|-
| <categorytree mode=pages hideroot="on" headings=bullet showcats=0 depth=4>Ressources</categorytree> ||<categorytree mode=pages hideroot="on" headings=bullet showcats=1 depth=4>Fablabs?</categorytree>
|}

{{#ask:[[Category:Thèmes]]
 |?Category
 |format=tagcloud
 |template=TagCloudTitle
 |headers=show
 |tagorder=alphabetical
 |widget=sphere
 |font=arial
 |link=all
 |height=220
 |width=200
}}

