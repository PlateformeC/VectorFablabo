__NOTOC__

== À Plateforme C ==

<div class="vf-liste-vignettes">
{{#ask:[[Localisation::PlateformeC]][[Category:CNC]]
 | ?
 | ?Type
 | ?Image
 |format=template |template=ListeVignettes |headers=hide |link=none  |offset= }}
</div>

== À l'atelier partagé du Breil ==

<div class="vf-liste-vignettes">
{{#ask:[[Localisation::PiNG]][[Category:CNC]]
 | ?
 | ?Type
 | ?Image
 |format=template |template=ListeVignettes |headers=hide |link=none  |offset= }}
</div>

