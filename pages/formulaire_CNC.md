<noinclude>
Ceci est le formulaire « CNC ».
Pour créer une page avec ce formulaire, entrez le nom de la page ci-dessous ;
si une page avec ce nom existe déjà, vous serez redirigé vers un formulaire pour l’éditer.


{{#forminput:form=CNC}}

</noinclude><includeonly>
<div id="wikiPreview" style="display: none; padding-bottom: 25px; margin-bottom: 25px; border-bottom: 1px solid #AAAAAA;"></div>
{{{for template|CNC}}}
{| class="formtable"
! Image :
|
| {{{field|image|input type=text with autocomplete|uploadable|values from namespace=File}}}
|-
! Type
| type de machine
| {{{field|type|input type=text|values from category=Machines(definitions)}}}
|-
! Fonction 
| (sert à ?)
| {{{field|fonction}}}
|-
! Dangerosité
| (un choix)
| {{{field|dangerosité|input type=listbox|values=peu,pas,assez,très}}}
|-
! Nombre
| (quantité de cet outil)
| {{{field|quantité|size=4}}}
|-
! Modèle
| (modèle de l'outil)
| {{{field|modèle|input type=text}}}
|-
! Statut 
| (en panne, fonctionnelle...)
| {{{field|statut|input type=listbox|values=fonctionnelle,en panne,en cours de mise au point}}}
|-
! Localisation
| (PiNG ou PlateformeC ?)
| {{{field|localisation|input type=text|values from category=lieux}}}
|-
! Entrée
| (format des fichiers sources)
| {{{field|entrées|list|delimiter=,|input type=text}}}
|-
! Sortie
| (ce que cette machine produit) 
| {{{field|sortie|list|delimiter=,|input type=text}}}
|-
! Ingredients
| (materiaux qu'elle utilise)
| {{{field|ingredients|list|delimiter=,|input type=text with autocomplete|values from category=Materiaux}}}
|-
! Largeur de travail 
| (cm)
| {{{field|largeur|size=4}}}
|-
! Longueur de travail
| (cm)
| {{{field|longueur|size=4}}}
|-
! Hauteur de travail 
| (cm)
| {{{field|hauteur|size=4}}}
|}

{{{end template}}}
{{DebugDatafab}}

'''Texte libre:'''

{{{standard input|free text|rows=10}}}


{{{standard input|summary}}}

{{{standard input|minor edit}}} {{{standard input|watch}}}

{{{standard input|save}}} {{{standard input|preview}}} {{{standard input|changes}}} {{{standard input|cancel}}}
</includeonly>
<noinclude>[[category:datafab]]</noinclude>